package com.example;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.producer.JmsProducer;


@RunWith(SpringRunner.class)
@SpringBootTest
public class JmsProducerTest {

    @Autowired
    private JmsProducer producer;
    
    @Test
    public void contexLoads() throws Exception {
        assertNotNull(producer);
    }

    @Test
    public void sendTest() {
        for (int i = 0; i < 10000; i++) {
            producer.send("Test message " + i);
        }
    }

}
