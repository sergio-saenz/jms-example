package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.example.config.ApplicationConfig;

@SpringBootApplication
@Import({ApplicationConfig.class})
public class BrokerApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(BrokerApplication.class, args);
    }

}
