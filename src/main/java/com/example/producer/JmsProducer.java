package com.example.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class JmsProducer {

    @Autowired
    JmsTemplate jmsTemplate;
    
    public void send (String message) {
        jmsTemplate.convertAndSend("jmsqueue", message);
    }
}
