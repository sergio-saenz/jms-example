package com.example.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class JmsConsumer {

    private static final Logger LOG = LoggerFactory.getLogger(JmsConsumer.class);

    @JmsListener(destination = "jmsqueue")
    public void receiveMessage (String message) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        LOG.info("Received <{}>", message);
    }
    
}
